## This program requires data from 02_zClustersDB_calculations.R
## later than git:
## Made by Oeyvind Oedegaard - oeyvind.oedegaard@embl.de

####-------------
## Package dependencies

#install.packages("ggplot2")
library("ggplot2")

#install.packages("rstudioapi")
library("rstudioapi")

library(devtools)
#devtools::install_github("cmartin/ggConvexHull")
library('ggConvexHull')

#install.packages("scatterplot3d")
library("scatterplot3d")

####-------------
## INPUT
parentPath = paste0(dirname(rstudioapi::getActiveDocumentContext()$path),"/ExampleData") # get the path of the script/git repo

# Experiment folder
DBSCAN_outputFolder = "/Raw_Images_IJ_20180206_DBSCAN_20180306_RDA"

# Experiment parameters
xy_pixelSize = 20
z_pixelSize  = 140
z_weight     = 8 # This is a measure of how many times the psf is sampled in z

# Select if you want to run the script and store the values in memory only, or if you want save all the outptdata in outPath
save_data = TRUE # Select true if you want to save the computed data on your computer

## Folder managements  	
# input
inputPath = paste0(parentPath , DBSCAN_outputFolder) # Folder with DBSCAN output  RDA files
binPath = paste0(parentPath , "/Bin_Masks" ) # Folder with all the binary masks
# output
outPath_plots = paste0(parentPath , gsub("_RDA","_plot_",DBSCAN_outputFolder), gsub("-","",Sys.Date())) # make an output folder name with date of computation

setwd(inputPath) # Set the working directory

# Make outPath_tables file path if it does not exist
if(save_data){
  if (file.exists(outPath_plots)){
  } else {
    dir.create(file.path(outPath_plots))
  }
}
####---------------------
## Read All RDA files
clusterSummary 	=	readRDS(file = "clusterSummary.rda")
DBSCAN_clusters	=	readRDS(file = "DBSCAN_clusters.rda")
regionSummary	=	readRDS(file = "regionSummary.rda")
spotlist_filtered_clustered	=	readRDS(file = "spotlist_filtered_clustered.rda")
#spotlist_per_cluster_annotated	=	readRDS(file = "spotlist_per_cluster_annotated.rda")

head(clusterSummary[[1]])
head(DBSCAN_clusters[[1]])
head(regionSummary)
head(spotlist_filtered_clustered[[1]])
#head(spotlist_per_cluster_annotated[[1]])





####---------------------
## Plotting
#i = 20
if(save_data){
  
  
  for(i in 1:length(spotlist_filtered_clustered)){
    filenName_i = regionSummary$fileName[i]
    ## get dimentions of original image from binary image
    image_dim = dim(EBImage::readImage(paste0(binPath , "/",filenName_i), as.is = TRUE))
    filename_trunkated = paste0(substr(filenName_i, 1, 50),"...")
    
    if(length(spotlist_filtered_clustered[[i]]$x..nm.[spotlist_filtered_clustered[[i]]$cluster>0])>0){ # avoid code crash if there are no localizations
      print(paste0("plotting: ", i))
      
      
      # ## plot 1
      # png(file=paste0(outPath_plots,"/",gsub(".tif", "", filenName_i), '_2dplot01.png'))
      #   		plot(spotlist_filtered_clustered[[i]]$x..nm.,
      #   		     spotlist_filtered_clustered[[i]]$y..nm.,
      #   		pch = 3,
      #   		ylim = c(max(spotlist_filtered_clustered[[i]]$y..nm.), min(spotlist_filtered_clustered[[i]]$y..nm.)),
      #   		col = spotlist_filtered_clustered[[i]]$cluster,
      #   		asp = TRUE)
      # dev.off()
      
      
      #plot 2D of each region
      
      # Separate the data from the background
      plot_list_i_bgRemoved = spotlist_filtered_clustered[[i]][spotlist_filtered_clustered[[i]]$cluster > 0,]
      plot_list_i_bg = spotlist_filtered_clustered[[i]][spotlist_filtered_clustered[[i]]$cluster == 0,]
      
      # convert the clusters into char
      cluster_char = paste0(plot_list_i_bgRemoved$cluster,"_")
      
      #plot
      
      #Make palette 
      default_palette = rep(c("red",     "green3",  "blue",    "cyan",    "magenta", "yellow",  "gray"),max(plot_list_i_bgRemoved$cluster)+1/7)
      p = ggplot() +
        geom_convexhull(data = plot_list_i_bgRemoved,
                        alpha = 0.9,
                        #color = "black",
                        aes(x = plot_list_i_bgRemoved$x..nm.,
                            y = plot_list_i_bgRemoved$y..nm.,
                            fill = cluster_char)) +
        geom_point(data = plot_list_i_bgRemoved,
                   shape = 3,
                   aes(x = plot_list_i_bgRemoved$x..nm.,
                       y = plot_list_i_bgRemoved$y..nm.,
                       col = cluster_char
                       )) +
        scale_colour_manual(values = default_palette) + 
        geom_point(data = plot_list_i_bg,
                   shape = 3,
                   color="black",
                   aes(x = plot_list_i_bg$x..nm.,
                       y = plot_list_i_bg$y..nm. )) +
        theme_classic() +
        theme(legend.position="none") +
        scale_y_reverse( lim=c(image_dim[2]*xy_pixelSize,0)) +
        scale_x_continuous(expand = c(0, 0), limits = c(0, image_dim[1]*xy_pixelSize)) +
        coord_fixed()
      
        
      # save as png
      png(file=paste0(outPath_plots,"/",gsub(".tif", "", filenName_i), '_2dplot02.png'))
      print(p)
      dev.off()
      p
      # save as pdf
      pdf(file=paste0(outPath_plots,"/",gsub(".tif", "", filenName_i), '_2dplot02.pdf'))
      print(p)
      dev.off()
      
      # plot in 3D
      display_angle = 90
      listPlot = spotlist_filtered_clustered[[i]]
      
      listPlot$z..nm. = listPlot$frame* z_pixelSize
      listPlot$clusterID = spotlist_filtered_clustered[[i]]$cluster
      
      coord3D = data.frame(xnm = listPlot$x..nm.,
                           ynm = listPlot$y..nm.,
                           znm = listPlot$z..nm.)
      
      png(file=paste0(outPath_plots,"/",gsub(".tif", "", filenName_i), '_3dplot01.png'))
      scatterplot3d(coord3D, pch = 3 ,angle = display_angle,  asp = FALSE)
      par(new=TRUE)
      scatterplot3d(coord3D, color = listPlot$clusterID,angle = display_angle,  asp = FALSE)
      dev.off()
      
      pdf(file=paste0(outPath_plots,"/",gsub(".tif", "", filenName_i), '_3dplot01.pdf'))
      scatterplot3d(coord3D, pch = 3 ,angle = display_angle,  asp = FALSE)
      par(new=TRUE)
      scatterplot3d(coord3D, color = listPlot$clusterID,angle = display_angle,  asp = FALSE)
      dev.off()
      
      
    }else{print(paste0("skipped plotting: ",i,": ",filenName_i))}
    
  }
}
