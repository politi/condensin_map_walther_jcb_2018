function [outStack] = fn_apply_shift(inStack, curXShift, curYShift, curZShift)
%This function apply shift to a 3D volume
%inStack: input stack
%outStack: output stack
%curXShift: translation in x 
%curYShift: translation in y 
%curZShift: translation in z

%Author: M. Julius Hossain, EMBL, Heidelberg
%Last update: 2018-03-06

outStack = inStack;
if curXShift >= 0 && curYShift >=0 && curZShift>=0
    outStack(1:end-curXShift,1:end-curYShift,1:end-curZShift) = inStack(1+curXShift:end,1+curYShift:end,1+curZShift:end);
elseif curXShift < 0 && curYShift >=0 && curZShift>=0
    outStack(1-curXShift:end,1:end-curYShift,1:end-curZShift) = inStack(1:end+curXShift,1+curYShift:end,1+curZShift:end);
elseif curXShift < 0 && curYShift <0 && curZShift>=0
    outStack(1-curXShift:end,1-curYShift:end,1:end-curZShift) = inStack(1:end+curXShift,1:end+curYShift,1+curZShift:end);
elseif curXShift < 0 && curYShift <0 && curZShift<0
    outStack(1-curXShift:end,1-curYShift:end,1-curZShift:end) = inStack(1:end+curXShift,1:end+curYShift,1:end+curZShift);
elseif curXShift >=0 && curYShift <0 && curZShift<0
    outStack(1:end-curXShift,1-curYShift:end,1-curZShift:end) = inStack(1+curXShift:end,1:end+curYShift,1:end+curZShift);
elseif curXShift >=0 && curYShift >=0 && curZShift<0
    outStack(1:end-curXShift,1:end-curYShift,1-curZShift:end) = inStack(1+curXShift:end,1+curYShift:end,1:end+curZShift);
elseif curXShift >= 0 && curYShift <0 && curZShift>=0
    outStack(1:end-curXShift,1-curYShift:end,1:end-curZShift) = inStack(1+curXShift:end,1:end+curYShift,1+curZShift:end);
elseif curXShift < 0 && curYShift>0 && curZShift<0
    outStack(1-curXShift:end,1:end-curYShift,1-curZShift:end) = inStack(1:end+curXShift,1+curYShift:end,1:end+curZShift);
end
end

