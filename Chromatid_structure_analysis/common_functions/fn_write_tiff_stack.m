function [flag] = fn_write_tiff_stack(imageStack, fp, fn, bitPerPixel)
% Write a 3D volume into a single tiff file
% imageStack: input 3D volume
% fp: file path
% fn: filename,
% bitPerPixel: bit depth
% flag: 1 saved successfully, 0 otherwise

flag = 0;
fullFilename = fullfile(fp,fn);
numFrames=size(imageStack, 3);

if numFrames < 1
    return;
end

switch bitPerPixel
    case 8
        imageStack = uint8(imageStack);
    case 16
        imageStack = uint16(imageStack);
    otherwise
        disp('Invalid bit depth - Line 24: fn_Write_TiffStack');
        % This can be modified using matlab tiff class for higher bit depth
        % http://www.mathworks.com/help/matlab/ref/tiffclass.html
        flag = 0;
        return;
end

if exist(fullFilename)
    disp([fullFilename ' is being updated...']);
end
%Write the first slice
imwrite(imageStack(:,:,1), fullFilename, 'tif', 'writemode', 'overwrite', 'Compression','none')
%Write subsequent silces
for i = 2:numFrames
    imwrite(squeeze(imageStack(:,:,i)), fullFilename, 'tif', 'writemode', 'append', 'Compression','none');
end

flag = 1;
end