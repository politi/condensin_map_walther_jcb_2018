This script allows the user to draw an irregular shape over a gray scale image in order to extract the region of interest and save the segmented result as a binary mask. 
The user can select a range of slices from a 3D stack to be segmented. This function pops up each slice of the input stack one by one to allow the user to draw the boundary of the region of interest. To do so, the user needs to keep the left button of the mouse holded while drawing a region of interest in one slice.
To skip drawing anything on a particular slice, please click on the left button of the mouse once (do not hold it). It saves a binary mask of the segmented image and also an overlay image. The script allows the user to update a few slices of the segmented stack which have already been segmented.

