function fn_manual_segmentation_main
%%
% This script allows user to draw an irregular shape over a gray scale 
% image to extract the region of interest.
% User can select a range of slices from a 3D stack to segment 
% This function pops up each slice of the input stack one by one to allow 
% user to draw the boundary.
% To skip drawing anything on a particular slice, please click on the left 
% button of the mouse once (do not hold it)
% It saves a binary mask of the segmented image and also overlaid image.
% The script allows user to update few slices of stack that has been 
% already segmented

% Author: Julius Hossain, EMBL, Heidelberg, Germany. Email: julius.hossain@embl.de
% Last update: 2018-02-26 (a bit of documentation added)
%%

clc;	
clear all;	
close all;

%This two parameters have to be changed if you don't want to segment/edit
%all slices
zinit = 1; %Select the index of starting slice that you want to segment
zfin  = 2; %Select the index of last slice that you want to segment

fontSize = 16; % Size of the font for the display texts
bitPerPixel = 16; % Bit depth for the stack to save
outBinDirSuffix = 'Segmented_BinMask'; % output directory for binary mask
outOvrDirSuffix = 'Segmented_Overlaid'; % output dir for overlaid image

%Defaul input directory - change it with the directory containing the data
inDir = 'C:\Data\Sted\Registered\';
% It will just look for the tif file in the default directory. 
% User can slect a file from the default directory or a diferent directory

% Select input tif stack
[filename, inDir] = uigetfile(fullfile(inDir, '*.tif'), 'Select a tif stack');
if isequal(filename,0)
    return;
end

fsIdx = find(inDir == filesep, 2, 'last');
%Directory to save segmented binary mask
outDirBinMask = fullfile(inDir(1:fsIdx(1)), outBinDirSuffix,  filesep);
%Directory to save original image overlaid the boundary of segmentation
outDirOverlaid  = fullfile(inDir(1:fsIdx(1)), outOvrDirSuffix, filesep);

%Create output directories
if ~exist(outDirBinMask)
    mkdir(outDirBinMask);
end
if ~exist(outDirOverlaid)
    mkdir(outDirOverlaid);
end

%Open tiff stack
[inStack] = fn_open_tiff_stack(inDir, filename);

% Check whether segmented results already exist or not
if ~exist([outDirBinMask, filename])
    binMask = zeros(size(inStack));
else
    binMask = fn_open_tiff_stack(outDirBinMask, filename); %zeros(size(inStack));
end

% Check whether overlaid images already exist or not
if ~exist([outDirOverlaid, filename])
    overlaidImg = inStack;
else
    overlaidImg = fn_open_tiff_stack(outDirOverlaid, filename); 
end

Nz = size(inStack, 3);
if zinit < 1
    zinit = 1;
end
if zfin>Nz
    zfin = Nz;
end

% Open image stacks slice by slice, allows user to draw the outline of the
% object of interest
for zplane = zinit: zfin
    grayImage = inStack(:,:,zplane);
    maxVal= max(inStack(:))+1;
    figHand = figure;imshow(grayImage, [0 maxVal]);
    axis on;
    title(['Slice ' num2str(zplane) ' of ' num2str(Nz) ' - please draw the outline of the object of interest'], 'FontSize', fontSize);
    set(gcf, 'Position', get(0,'Screensize'));
    
    if zplane == 1
        message = 'Click left button of the mouse, hold it while drawing and release it when you are done';
        uiwait(msgbox(message));
    end
    hFH1 = imfreehand();
    % Create the segmented binary mask from the object drawn
    binMask(:,:,zplane) = hFH1.createMask();
  
    %Extract the boundaries from the bin mask and draw it on the original
    %images
    [B,L] = bwboundaries(binMask(:,:,zplane),8,'noholes');
    for idx = 1:size(B)
        for k = 1:length(B{idx})
            grayImage(B{idx}(k,1), B{idx}(k,2)) = maxVal;
        end
    end
    overlaidImg(:,:,zplane) = grayImage;
    close all;
end
% Save the binary mask
flag = fn_write_tiff_stack(binMask,     outDirBinMask,  filename, bitPerPixel);
if flag == 0
    disp('Stack containing binary mask cannot be saved');
else
    disp('Stack containing binary mask has been saved');
end
% Save overlaid images 
flag = fn_write_tiff_stack(overlaidImg, outDirOverlaid, filename, bitPerPixel);
if flag == 0
    disp('Stack containing original image overlaid segmentation mask cannot be saved');
else
    disp('Stack containing overlaid image has been saved');
end
