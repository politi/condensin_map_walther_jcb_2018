%This script calculates the distance of a spot cluster to chromsome central
%axis
%Author: Julius Hossain, EMBL Heidelberg.
%Last update: 2017-11-07

voxelSizeXYnm = 20;
voxelSizeZnm = 140;

spotClustDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Registered_Out_20171027_R_Out_v01', filesep);
spotAxisDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Central_axis', filesep);
projImgDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Processed_cond_volume\Reg_Max_Proj', filesep);
projBinMaskDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Proj_bin_mask',filesep);
outDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Spot_to_CA_dist_indiv_region', filesep);
outDirCentAxis = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\Central_axis_with_spot', filesep);

if ~exist(outDir)
    mkdir(outDir);
end
if ~exist(outDirCentAxis)
    mkdir(outDirCentAxis);
end

histStep =20;
histMax = 1200;
fnList = dir([spotClustDir '*clusterTable.txt']);

for fileIdx = 1:length(fnList)
    tableSpot = readtable([spotClustDir fnList(fileIdx).name]);
    yCoords_pix = tableSpot.x__nm_/voxelSizeXYnm; %Python and Matlab swap x and y coords
    xCoords_pix = tableSpot.y__nm_/voxelSizeXYnm;
    zCoords_pix = tableSpot.z__nm_/voxelSizeZnm;
    
    tableAxis = readtable([spotAxisDir fnList(fileIdx).name(1:end-24) '.txt']);
    centAxisX_pix = tableAxis.centAxisX_pix;
    centAxisY_pix = tableAxis.centAxisY_pix;
    centAxisZ_pix = tableAxis.centAxisZ_pix;
        
    numClust = length(xCoords_pix);
    
    curOrigImg = imread([projImgDir fnList(fileIdx).name(1:end-20)]);
    curBinImg = imread([projBinMaskDir fnList(fileIdx).name(1:end-20)]);
    h = figure('NumberTitle','off', 'Visible','off'); imagesc(curOrigImg); axis equal; axis tight;  colormap(gray)
    hold on; 
    plot(centAxisY_pix, centAxisX_pix, 'y', 'LineWidth', 2);
    plot(yCoords_pix, xCoords_pix, '+m', 'MarkerSize', 10, 'LineWidth', 2);
    set(gcf, 'Units', 'Normalized', 'OuterPosition', [0, 0.04, 1, 0.96]);
    saveFilename = fullfile(outDirCentAxis, fnList(fileIdx).name(1:end-20));
    saveas(h, saveFilename, 'tif');
    
    distImg = zeros(size(curBinImg));
    for i = 1: length(centAxisX_pix)
        distImg(round(centAxisX_pix(i)), round(centAxisY_pix(i))) = 1;
    end
    sepBinImg = distImg;
    distImg = bwdist(distImg);
    
    sepBinImg = imdilate(sepBinImg, strel('diamond',2));
    lblImg = curBinImg;
    lblImg(sepBinImg == 1) = 0;
    twoDLabel = bwconncomp(lblImg);
    numPixels = cellfun(@numel,twoDLabel.PixelIdxList);
    [~,idx] = max(numPixels);
    lblImg(:,:) = 0;
    lblImg(twoDLabel.PixelIdxList{idx}) = 1;
    if twoDLabel.NumObjects > 1
        numPixels(idx) = 0;
        [~,idx] = max(numPixels);
        lblImg(twoDLabel.PixelIdxList{idx}) = 2;
    end
    
    clustCADist_pix = zeros(numClust,1);
    clustLabel = zeros(numClust,1);
    for i = 1: numClust
        clustCADist_pix(i) = distImg(round(xCoords_pix(i)), round(yCoords_pix(i)));
        clustLabel(i)  = lblImg(round(xCoords_pix(i)), round(yCoords_pix(i)));
    end
    clustCADist = clustCADist_pix * voxelSizeXYnm;
    meanDist_nm = mean(clustCADist);
    close all;
    histDist = histc(clustCADist, 1:histStep:histMax);
    h = figure('NumberTitle','off', 'Visible','off'); bar(1:histStep:histMax, histDist);
    filename = [outDir  fnList(fileIdx).name(1:end-20)];
    saveas(h, filename, 'tif');
    filename = [outDir  fnList(fileIdx).name(1:end-24) '.mat'];
    save(filename, 'histDist', 'histStep', 'histMax', 'xCoords_pix', 'yCoords_pix', 'zCoords_pix', 'clustCADist_pix', 'clustLabel', 'meanDist_nm');
    
    close all;
end

    
