%This script processes all chromatid regions by calling the 
%function (fn_generate_max_projected_condensin) that generates the max 
%projected binmask for each indidividual condensin regions.

%Author: M. Julius Hossain, EMBL
%Last update: 2017_09_14

clear all;
clc;
%Go two step upper parent directory to get the source root directory
[parentdir,~,~]=fileparts(pwd);
[src_root_dir,~,~]=fileparts(parentdir);
addpath(fullfile(src_root_dir, 'common_functions'));

%Pixel sizes in micrometer
voxelSizeX = 0.02;
voxelSizeY = 0.02;
voxelSizeZ = 0.14;
%Root directory containing input data
inDirRoot = fullfile('X:\Nike\Abberior-STED\RegionsAnalysis_SingleColour\Results', filesep);
%Root directory for output
outDirRoot = 'Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_CA_Dist\';
regionDir = dir([inDirRoot '*Drift*']);
%Process individual region one by one
for regionDirIdx = 1: length(regionDir)
    if regionDir(regionDirIdx).isdir == 1
        curRegionDir = fullfile(inDirRoot, regionDir(regionDirIdx).name, filesep);
        %try
            disp(['Processing: ' regionDir(regionDirIdx).name]);
            fn_generate_max_projected_condensin(curRegionDir, outDirRoot);
%         catch
%             disp(['Could not process: '  regionDir(regionDirIdx).name]);
%         end
    end
end
