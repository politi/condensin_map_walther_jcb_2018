%This script combines spot nearest neigbour distance calculated for all
%individual regions

%Author: Julius Hossain, EMBL Heidelberg.
%Last update: 2011-11-07

inDirRoot  = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_NN_Dist', filesep);
outDirRoot = fullfile(inDirRoot,'Combined_nn_results', filesep);

%
subDir = {'2D', '3D'};

%name of different sub-units
protNameSpot = {'Cond-KIF4A', 'Cond-TOP2A', 'Cond2-gfpNCAPH2', 'Cond1-D2', 'Cond2-D3', 'Cond2-H2', 'Cond1-H', 'Cond12-SMC4'};
protNameSingCol = {'gfpKIF4Ac173', 'gfpTOP2Ac102', 'gfpNCAPH2c1', 'NCAPD2gfpc272c78', 'NCAPD3gfpc16', 'NCAPH2gfpc67', 'NCAPHgfpc86', 'SMC4gfpz82z68', };

%Name of mitotic phase
phaseName = {'prometa', 'ana_early', 'ana-early'}; %both 'ana_early' and 'ana-early' were used to deal with the typos in the input file

flagName = {'flag_04'}; %Just to analyse different type of data separately

for subDirIdx = 1: length(subDir)
    %Generate input and output filenames
    inDir = fullfile(inDirRoot, subDir{subDirIdx}, filesep);
    outDir = fullfile(outDirRoot, subDir{subDirIdx}, filesep);
    
    %Create output directory
    if ~exist(outDir)
        mkdir(outDir);
    end
    
    %Combine results for each sub-unit belonging to a particular mitotic
    %phase
    for flagIdx = 1: length(flagName)
        for protIdx = 1: length(protNameSpot)
            for phaseIdx = 1: length(phaseName)
                regionDir = dir([inDir '*' protNameSpot{protIdx} '*' phaseName{phaseIdx} '*' flagName{flagIdx} '*.mat']);
                numRegions = length(regionDir);
                if numRegions == 0
                    continue;
                end
                nnDist = zeros(numRegions,1);
                for fileIdx = 1:length(regionDir)
                    curMat = load([inDir regionDir(fileIdx).name]);
                    
                    if fileIdx == 1
                        histDist = curMat.histDist;
                        histStep = curMat.histStep;
                        histMax = curMat.histMax;
                    else
                        histDist = histDist + curMat.histDist;
                    end
                    nnDist(fileIdx) = curMat.meanDist;
                end
                meanDist_nm = mean(nnDist);
                medDist_nm = median(nnDist);
                stdDist_nm = std(nnDist);
                h = figure('NumberTitle','off', 'Visible','off'); bar(1:histStep:histMax, histDist);
                filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.tif'];
                saveas(h, filename, 'tif');
                %filename = [outDir  protNameSpot{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.mat'];
                %save(filename, 'histDist', 'histStep', 'histMax');
                filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.txt'];
                hist_nndist_step_20nm = histDist;
                %num_of_regions = zeros(length(histDist), 1);
                %num_of_regions(:) = numRegions;
                tableHist = table(hist_nndist_step_20nm);
                writetable(tableHist,filename, 'Delimiter','\t');
                
                filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Dist_mean-median_stdev.txt'];
                
                tableParams = table(meanDist_nm, medDist_nm, stdDist_nm);
                writetable(tableParams,filename, 'Delimiter','\t');
                
                close all;
            end
        end
    end
end