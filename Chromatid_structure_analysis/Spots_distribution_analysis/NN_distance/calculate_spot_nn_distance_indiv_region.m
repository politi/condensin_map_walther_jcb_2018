%This script takes spot coordinates as input and  calculates their nearest 
%neighbour distance.

%Author: Julius Hossain, EMBL Heidelberg.
%Last update: 2011-11-07

[src_root_dir,~,~]=fileparts(pwd);
%Include directories containing necessary files
addpath(fullfile(src_root_dir, 'external_functions'));

%Root directory for spot data analysis
inDirRoot = 'Z:\Julius\Test_Processing\Spot_analysis\';
%Spot input directory
spotClustDir = fullfile(inDirRoot, 'Registered_Out_20171027_R_Out_v01', filesep);
%Root output directory to store spot nearest neighbour distances
outDirRoot = fullfile(inDirRoot, 'Spot_clust_NN_Dist', filesep);

%output directory with 2d nn distance
outDir2D = fullfile(outDirRoot, '2D', filesep);
if ~exist(outDir2D)
    mkdir(outDir2D);
end
%output directory for 3D nn distance 
outDir3D = fullfile(outDirRoot, '3D', filesep);
if ~exist(outDir3D)
    mkdir(outDir3D);
end

histStep = 20; %Step size of histogram
histMax = 1200;%maximum value of histogram

%Name of the file containing spot coordinates
fnList = dir([spotClustDir '*clusterTable.txt']);

%Process individual files
for fileIdx = 1:length(fnList)
    curTable = readtable([spotClustDir fnList(fileIdx).name]);
    xCoords = curTable.x__nm_;
    yCoords = curTable.y__nm_;
    zCoords = curTable.z__nm_;
    
    numClust = length(xCoords);
    distMat3D = zeros(numClust, 3);
    clustNNDist3D = zeros(numClust, 1);
    clustNNIdx3D = zeros(numClust, 1);
    distMat3D(:,1) = xCoords; 
    distMat3D(:,2) = yCoords; 
    distMat3D(:,3) = zCoords; 
    
    %Get nearest pairwise distance 3D
    intPtDistMat3D = ipdm(distMat3D);
    intPtDistMat3D(intPtDistMat3D == 0) = Inf;
    for clustIdx = 1: length(xCoords)
        [clustNNDist3D(clustIdx), clustNNIdx3D(clustIdx)] = min(intPtDistMat3D(clustIdx,:));
    end  
    %Get mean and median
    meanDist = mean(clustNNDist3D);
    medDist = median(clustNNDist3D);
    %Generate histogram
    histDist = histc(clustNNDist3D, 1:histStep:histMax);
    %Display the histogram
    h = figure('NumberTitle','off', 'Visible','off'); bar(1:histStep:histMax, histDist);
    filename = [outDir3D  fnList(fileIdx).name(1:end-20)];
    saveas(h, filename, 'tif');
    filename = [outDir3D  fnList(fileIdx).name(1:end-24) '.mat'];
    save(filename, 'histDist', 'histStep', 'histMax', 'meanDist', 'medDist');
    
    
    distMat2D = zeros(numClust, 2);
    clustNNDist2D = zeros(numClust, 1);
    clustNNIdx2D = zeros(numClust, 1);
    distMat2D(:,1) = xCoords; 
    distMat2D(:,2) = yCoords; 
    %Get nearest pairwise distance 3D
    intPtDistMat2D = ipdm(distMat2D);
    intPtDistMat2D(intPtDistMat2D == 0) = Inf;
    for clustIdx = 1: length(xCoords)
        [clustNNDist2D(clustIdx), clustNNIdx2D(clustIdx)] = min(intPtDistMat2D(clustIdx,:));
    end
    %Get mean and median
    meanDist = mean(clustNNDist2D);
    medDist = median(clustNNDist2D);
    %Generate histogram
    histDist = histc(clustNNDist2D, 1:histStep:histMax);
    %Display the histogram and save the files
    h = figure('NumberTitle','off', 'Visible','off'); bar(1:histStep:histMax, histDist);
    filename = [outDir2D  fnList(fileIdx).name(1:end-20)];
    saveas(h, filename, 'tif');
    filename = [outDir2D  fnList(fileIdx).name(1:end-24) '.mat'];
    save(filename, 'histDist', 'histStep', 'histMax', 'meanDist', 'medDist');
    
    close all;
    clear distMat3D;
    clear clustNNDist3D;
    clear clustNNIdx3D;
    clear distMat2D;
    clear clustNNDist2D;
    clear clustNNIdx2D;
end

    
