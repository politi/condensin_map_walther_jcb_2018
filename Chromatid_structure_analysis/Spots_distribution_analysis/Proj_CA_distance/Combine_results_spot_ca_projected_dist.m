
%This scripts combines the results of projected distances per condensin
%subunit and mitotic phase

%Author: Julius Hossain, EMBL, Heidelberg
%Last update: 2018_03_05

clc;
clear all;
%Input directory
inDir  = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_Proj_CA_Dist\Dist_between_projected_spots_indiv_regions', filesep);
%Output directory
outDir = fullfile('Z:\Julius\Test_Processing\Spot_analysis\Spot_clust_Proj_CA_Dist\Dist_between_projected_spots_combined', filesep);

%Create output directory
if ~exist(outDir)
    mkdir(outDir);
end

%Name of different subunits
protNameSpot = {'Cond-KIF4A', 'Cond-TOP2A', 'Cond2-gfpNCAPH2', 'Cond1-D2', 'Cond2-D3', 'Cond2-H2', 'Cond1-H', 'Cond12-SMC4'};
protNameSingCol = {'gfpKIF4Ac173', 'gfpTOP2Ac102', 'gfpNCAPH2c1', 'NCAPD2gfpc272c78', 'NCAPD3gfpc16', 'NCAPH2gfpc67', 'NCAPHgfpc86', 'SMC4gfpz82z68', };

%Name of mitotic phases
phaseName = {'prometa', 'ana_early', 'ana-early'}; %The third is to deal with the erron in filename
flagName = {'flag_04'};

%Process all results of all regions one by one
for flagIdx = 1: length(flagName)
    for protIdx = 1: length(protNameSpot)
        for phaseIdx = 1: length(phaseName)
            regionDir = dir([inDir '*' protNameSpot{protIdx} '*' phaseName{phaseIdx} '*' flagName{flagIdx} '*.mat']);
            numRegions = length(regionDir);
            if numRegions == 0
                continue;
            end
            caDist = zeros(numRegions,1);
            for fileIdx = 1:length(regionDir)
                curMat = load([inDir regionDir(fileIdx).name]);
                
                if fileIdx == 1
                    histDist = curMat.histDist;
                    histStep = curMat.histStep;
                    histMax  = curMat.histMax;
                else
                    histDist = histDist + curMat.histDist;
                end
                caDist(fileIdx) = curMat.meanDist_nm;
            end
            meanDist_nm = mean(caDist);
            medDist_nm = median(caDist);
            stdDist_nm = std(caDist);
            h = figure('NumberTitle','off', 'Visible','off'); bar(1:histStep:histMax, histDist);
            filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.tif'];
            saveas(h, filename, 'tif');
            %filename = [outDir  protNameSpot{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.mat'];
            %save(filename, 'histDist', 'histStep', 'histMax');
            filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Hist_xAxisInNanoMetre.txt'];
            hist_nndist_step_20nm = histDist;
            %             num_of_regions = zeros(length(histDist), 1);
            %             num_of_regions(:) = numRegions;
            tableHist = table(hist_nndist_step_20nm);
            writetable(tableHist,filename, 'Delimiter','\t');
            
            filename = [outDir  protNameSingCol{protIdx} '_' phaseName{phaseIdx} '_' flagName{flagIdx} '_Dist_mean-median_stdev.txt'];
            tableParams = table(meanDist_nm, medDist_nm, stdDist_nm);
            writetable(tableParams,filename, 'Delimiter','\t');
            
            close all;
        end
    end
end