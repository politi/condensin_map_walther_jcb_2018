function [maskLeft, maskRight, maskMiddle] = fn_detect_intensity_three_regions(dxVol, dyVol, NzVol, radEll)
%radEll = 35;
inImage = zeros(dxVol,dxVol,NzVol);
dy = size(inImage, 2);
[outImage] = fn_DrawSphere(inImage, ceil(dxVol/2), ceil(dy/2), ceil(NzVol/2), radEll, 1,1, 1, 0);
mask = zeros(dxVol, dyVol, NzVol);
mask(:,1,:) = outImage(:,ceil(dy/2),:);
for i = 2:dyVol
    mask(:,i,:) = mask(:,1,:);
end

maskLeft = mask;
maskLeft(floor(round(dxVol/2)-radEll*0.4):end,:,:) = 0;
%figure; imagesc(squeeze(mask1));

maskRight = mask;
maskRight(1:ceil(round(dxVol/2)+radEll*0.4),:,:) = 0;
%figure; imagesc(squeeze(mask2));

maskMiddle = mask;
maskMiddle(maskLeft == 1) = 0;
maskMiddle(maskRight == 1) = 0;
end

