function [ isoConDist, isoConChrMask, halfLineProf] = fn_get_isotropic_dist(conCrosDist, initCent, curRad, isoDx, isoDy, upSampleFactor)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
sumProjX = sum(conCrosDist);
[~, centX] = max(sumProjX);

sumProjY = sum(conCrosDist, 2);
[~, centY] = max(sumProjY);

% [xShift, yShift] = fn_determine_shift_for_refined_centre_of_mass(conCrosDist, initCent(1), initCent(2), curRad, curRad, 7, 7);
% centX = initCent(1)+ xShift;
% centY = initCent(2)+ yShift;

ellMinPosX = centX-curRad;
ellMinPosY = centY-curRad;

%h = figure('Name','Mean cross section intensity distribution of condensin','NumberTitle','off', 'Visible', 'off'); imagesc(conCrosDist); axis equal;
%hEllipse = imellipse(gca,[ellMinPosY ellMinPosX curRad*2 curRad*2]);

h = figure('Name','Mean cross section intensity distribution of condensin','NumberTitle','off', 'Visible', 'off'); imagesc(conCrosDist); axis equal;
hEllipse = imellipse(gca,[ellMinPosY ellMinPosX curRad*2+1 curRad*2+1]);
binaryImage = hEllipse.createMask();
cropConDist = conCrosDist;
cropConDist(binaryImage == 0) = 0;

[isoConDist, isoConChrMask, halfLineProf] = fn_nonistropic_to_isotrpic_distribution(cropConDist, centX, centY, isoDx, isoDy, upSampleFactor, curRad);

end

