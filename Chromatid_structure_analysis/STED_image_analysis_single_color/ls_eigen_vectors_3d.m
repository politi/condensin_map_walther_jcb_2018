function [V, R, xg, yg, zg] = ls_eigen_vectors_3d(x, y, z, voxelSizeX, voxelSizeY, voxelSizeZ)
%Compute Eigen vectors from 3d points represented by x,y and z
%Returns vectors -V, values - R, and centroids xg,yg and zg
xsp=x*voxelSizeX;
ysp=y*voxelSizeY;
zsp=z*voxelSizeZ;
xg=mean(xsp);
yg=mean(ysp);
zg=mean(zsp);
Txx=mean(xsp.^2)-xg^2;
Tyy=mean(ysp.^2)-yg^2;
Tzz=mean(zsp.^2)-zg^2;
Txy=mean(xsp.*ysp)-xg*yg;
Txz=mean(xsp.*zsp)-xg*zg;
Tyz=mean(ysp.*zsp)-yg*zg;
Tg=[Txx,Txy,Txz;Txy,Tyy,Tyz;Txz,Tyz,Tzz];

[V,R]=eig(Tg);
end