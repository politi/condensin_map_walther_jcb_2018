## Chromatid_structure_analysis
This code is used for the analysis of mitotic chromatid structure.

### Manual-segmentation_tiffstack
This script allows the user to draw an irregular shape over a gray scale image in order to extract the region of interest and save the segmented result as a binary mask.

### STED_image_analysis_single_colour
This module performs structural analyses on condensin regions: width and radial profile.

### STED_image_analysis_double_colour
This module performs a colocalization analysis between two condensin channels. 

### Spots_distribution_analysis
This module calculates NN, CA, AS, of Condensin subunits based on the coordinates of spots detected and clustered with zStackSpotPicker.

### Measuring_total_chromatid_length
This function calculates the total chromatid length of a cell by combining manual and automated segmentation.

### common_functions
Contains some common functions in different modules.

### external_functions
This folder contains functions from external sources which are available in public and have been used in some of the modules.