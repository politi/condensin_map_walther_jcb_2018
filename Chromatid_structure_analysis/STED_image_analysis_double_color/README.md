This module performs a colocalization analysis between two condensin channels of a 2D STED z-stack. 
It first segments the condensin regions from each channel by combining both local and global thresholding approaches. It computes the percentage of non-overlapping condensin regions between the two channels. It also calculates the normalized correlation between the two channels.

Note: The condensin region in one channel needs to be segmented manually first. 
The shift between the two channels (if any) needs to be corrected before applying 
this script.