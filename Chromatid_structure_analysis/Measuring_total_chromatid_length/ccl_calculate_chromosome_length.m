function ccl_calculate_chromosome_length(inDirRoot, outDir, fIndex, options)
% This function caculates whole chromatid length by combining manual and
% automated segmentation.
% Author: M. Julius Hossain, EMBL
% Last update: 2018_02_28

%%
% inDirRoot: root directory of input data
% fIndex: index/serial number of input data - used to select the row in the
%          output excel file
% options: running options and parameters value

clc;
close all;

%Directories cotaining raw data and segmented condensin data
inDirCon = fullfile(inDirRoot, options.conDir, filesep);
inDirSeg = fullfile(inDirRoot, options.manSegDir, filesep);
outDirSegVis = fullfile(outDir, options.segVisDir, filesep);
%Return if input directories are not found
if ~exist(inDirCon) || ~exist(inDirSeg)
    return;
end

% Get the name of input files
fnConList = dir([inDirCon '*Cond*.tif']);
fnNucList = dir([inDirCon '*DNA*.tif']);

% Generate results directory by adding current date at the end
fnCon = fnConList(1).name;
fnNuc = fnNucList(1).name;
sIdx = find(inDirCon == filesep, 3,'last');
%dateStr = date;
%outDir    = [inDirCon(1:sIdx(1)) 'Results_' dateStr(8:11) '_' dateStr(4:6) '_' dateStr(1:2) filesep];
% output directory to store shift corrected results - optional
%outDirShiftCorr = [outDir inDirCon(sIdx(1)+1:sIdx(2))];
%output xls file to store length measurement results
xlsFileName = [outDir 'Length_measurments.xls'];

% name of current region
regionName = inDirCon(sIdx(1)+1:sIdx(2)-1);

%create output directories if not exist
if ~exist(outDir)
    mkdir(outDir);
end
if ~exist(outDirSegVis)
    mkdir(outDirSegVis);
end
% if ~exist(outDirShiftCorr)
%     mkdir(outDirShiftCorr);
% end

% get meta data
tifInfo = imreadBFmeta([inDirCon fnCon]); %Read tif header
dxOrig = tifInfo.height; %Image height
dyOrig = tifInfo.width; % Image width
NzOrig = tifInfo.zsize; %Number of Z slices in lsm file

% change the volex size and image size for low resolution processing
if options.lowRes == 1
    options.voxelSizeX = options.voxelSizeX*options.dSamp;
    options.voxelSizeY = options.voxelSizeY*options.dSamp;
    dx = round(dxOrig/options.dSamp);
    dy = round(dyOrig/options.dSamp);
else
    dx = dxOrig;
    dy = dyOrig;
end

tpoint = options.tinit;

%Read condensin data
conStackOrig  = imreadBF([inDirCon fnCon],options.zinit:NzOrig,tpoint,1);
nucStackOrig  = imreadBF([inDirCon fnNuc],options.zinit:NzOrig,tpoint,1);
%Read manually segmented portion of condensin saved as a binary mask
[conRegionSegOrig] = fn_open_tiff_stack(inDirSeg, fnCon);

disp(['Processing: ' num2str(tpoint)]);

% Resample the data if processing is to be done in low resolution mode
if options.lowRes == 1
    if tpoint == options.tinit
        conStackLow = zeros(dx, dy, NzOrig);
    end
    for zplane=1:NzOrig
        conStackLow(:,:,zplane) = imresize(conStackOrig(:,:,zplane), 1/options.dSamp, 'bicubic');
        nucStackLow(:,:,zplane) = imresize(nucStackOrig(:,:,zplane), 1/options.dSamp, 'bicubic');
    end
    clear conStackOrig;
else
    conStackLow = conStackOrig;
    nucStackLow = nucStackOrig;
end

% change the voxel size for low resolution processing
if options.lowRes == 1 && tpoint == options.tinit
    options.voxelSizeX = options.voxelSizeX*options.dSamp;
    options.voxelSizeY = options.voxelSizeY*options.dSamp;
end
Nz = NzOrig;
%voxelSize = options.voxelSizeX*options.voxelSizeY*options.voxelSizeZ;

% Subtrack the default offset in the original image
conStack = conStackLow-options.intOffset;
nucStack = nucStackLow-options.intOffset;
conStackOrig = conStackOrig - options.intOffset;
nucStackOrig = nucStackOrig - options.intOffset;
conStack(conStack<0)= 0;
nucStack(nucStack<0)= 0;

conStackFilt = conStack;
conRegion = zeros(size(conStack));
nucStackFilt = nucStack;
nucRegion = zeros(size(nucStack));

%Apply Gaussian in 2D
for zplane = 1: Nz
    conStackFilt(:,:,zplane) = imgaussian(conStack(:,:,zplane), options.sigmaCon, options.hSizeCon);
    nucStackFilt(:,:,zplane) = imgaussian(nucStack(:,:,zplane), options.sigmaNuc, options.hSizeNuc);
end
%Get both 2D and 3D thresholds
[conThresh3D, conThresh2D] = fn_get_thresholds(conStackFilt, 0);
[nucThresh3D, nucThresh2D] = fn_get_thresholds(nucStackFilt, 0);

%Binarize the data based on 2D and 3D thresholds
for zplane = 1:Nz
    conRegion(:,:,zplane) =  double(conStackFilt(:,:,zplane) >= (conThresh3D * (1-options.bFactor2D) + conThresh2D(zplane) * options.bFactor2D));
    nucRegion(:,:,zplane) =  double(nucStackFilt(:,:,zplane) >= (nucThresh3D * (1-options.bFactor2D) + nucThresh2D(zplane) * options.bFactor2D));
end

% dilate the binary mask bit to make sure that it does not miss signal
% inside condensin/dna region
for zplane=1:Nz
    conRegion(:,:,zplane) = imdilate(conRegion(:,:,zplane), options.se2d);
    nucRegion(:,:,zplane) = imdilate(nucRegion(:,:,zplane), options.se2d);
    
end

% Get the background intensity of condensin and subtract it from the condensin signal
[avgBackIntCon, ~, ~] = qcs_calculate_background_intensity(conRegion, conStack, options.dil1Con, options.dil2Con);
conStack = conStack - avgBackIntCon;
conStack(conStack<0) = 0;

% Get the background intensity of dna and subtract it from dna signal
[avgBackIntNuc, ~, ~] = qcs_calculate_background_intensity(nucRegion, nucStack, options.dil1Chr, options.dil2Chr); %Used 1 pixel less than cond as DNA is thicker
nucStack = nucStack - avgBackIntNuc;
nucStack(nucStack<0) = 0;

% Caculate shift between condensin and DNA if computeShift is set to 1
if options.computeShift == 1
    [options.xShift, options.yShift, options.zShift] = fn_determine_shift_between_channels(nucStack, conStack, options.maxShiftXY, options.maxShiftZ);
end

% Apply shift to condensin channel - filtered image
[conStack] = fn_apply_shift(conStack, options.xShift, options.yShift, options.zShift);
% Apply shift to the segmented portion of condensin (binary mask)
[conRegionSegOrig] = fn_apply_shift(conRegionSegOrig, options.xShift, options.yShift, options.zShift);
% Apply shift to condensin channel - original image
[conStackOrig] = fn_apply_shift(conStackOrig, options.xShift, options.yShift, options.zShift);

% Save shift corrected files - optional
%fn_write_tiff_stack(nucStackOrig, outDirShiftCorr, fnNuc, options.bitDepth);
%fn_write_tiff_stack(conStackOrig, outDirShiftCorr, fnCon, options.bitDepth);

% combine both segmented results to define dna region to make sure that no
% signal is missed
nucRegion = double(or(nucRegion,conRegion));
for zplane=1:Nz
    nucRegion(:,:,zplane) = imdilate(nucRegion(:,:,zplane), options.se2d);
end

% Get total signal of condensin
totConInt = sum(sum(sum(conStack(nucRegion >0))));

%%
% Calculate length the segmented portion of the condensin
conRegionSeg = conRegionSegOrig;
conRegionSeg(conRegionSeg>0) = 1;
%Get the boundary points
conRegionSegBound = imsubtract(conRegionSeg, imerode(conRegionSeg, options.se3d));
conRegionSegBound(conRegionSegBound>0) = 1;
[x,y,z] = ls_threed_coord(find(conRegionSegBound),dx,dy);
x = x* options.voxelSizeX;
y = y* options.voxelSizeY;
z = z* options.voxelSizeZ;
boundPoints = [x y z];
intDist = ipdm(boundPoints);
%Start with two points sitting at maximum distance;
[~, linIdx] = max(intDist(:));
[ptIdx1, ptIdx2] = ind2sub(size(intDist),linIdx);

% Generate vector using these two points
conVect = [x(ptIdx1) - x(ptIdx2) y(ptIdx1) - y(ptIdx2) z(ptIdx1) - z(ptIdx2)];
normConVect = norm(conVect);
conAxis = conVect/normConVect;
numPoints = floor(normConVect/options.voxelSizeX);
xConPoints = zeros(numPoints+1,1);
yConPoints = zeros(numPoints+1,1);
zConPoints = zeros(numPoints+1,1);

%Get all the points along the vector
i = 1;
zFactor = round(options.voxelSizeZ/options.voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZCrop = options.voxelSizeZ/zFactor;
for alpha = 0:1/numPoints:1
    xConPoints(i) = x(ptIdx1)/options.voxelSizeX*(1-alpha) + x(ptIdx2)/options.voxelSizeX*alpha;
    yConPoints(i) = y(ptIdx1)/options.voxelSizeY*(1-alpha) + y(ptIdx2)/options.voxelSizeY*alpha;
    zConPoints(i) = z(ptIdx1)/voxelSizeZCrop*(1-alpha) + z(ptIdx2)/voxelSizeZCrop*alpha;
    i = i + 1;
end

%Crop the portion of segmented section to process it as an isotropic volume
maxX = round(max(x)/options.voxelSizeX);
minX = round(min(x)/options.voxelSizeX);
maxY = round(max(y)/options.voxelSizeY);
minY = round(min(y)/options.voxelSizeY);
maxZ = round(max(z)/options.voxelSizeZ);
minZ = round(min(z)/options.voxelSizeZ);

%dxCrop = maxX-minX+1;
%dyCrop = maxY-minY+1;
%NzCropOrig = maxZ-minZ+1;
%cropVolOrig = zeros(dxCrop, dyCrop, NzCropOrig);
cropVolOrig = conRegionSegOrig(minX:maxX,minY:maxY,minZ:maxZ);

%NzCrop = NzCropOrig * zFactor -zFactor +1;
cropVol   = ls_gen_intermediate_slices(cropVolOrig, zFactor);
xConPoints = round(xConPoints - minX + 1);
yConPoints = round(yConPoints - minY + 1);
minZ = round(min(z)/voxelSizeZCrop);
zConPoints = round(zConPoints - minZ + 1);
% Get centroids along DNA axis
[refCentX, refCentY, refCentZ] = extract_centroids_along_chromosome_axis(cropVol, xConPoints, yConPoints, zConPoints, conAxis(1), conAxis(2), conAxis(3), options.radius);

%Calculate the length of the segmented chromatid regions in pixels
segConLen = 0;
for i = 1: options.mesStep: numPoints
    j = i + options.mesStep;
    if j>numPoints +1
        j = numPoints + 1;
    end
    
    segConLen = segConLen + norm([refCentX(i)-refCentX(j) refCentY(i)-refCentY(j) refCentZ(i)-refCentZ(j)]);
    segConLen = segConLen + 1; % Just to add the value that was removed from the subtraction
end

%Convert it to micrometer (voxel sizes are isotropic here)
segConLen = segConLen*options.voxelSizeX;
%Calculate the total intensity on the segmented region
segConInt = sum(sum(sum(conStack(conRegionSeg>0 & nucRegion > 0))));
%Get the ration between total and segmented intensities
totSegRatio = totConInt/segConInt;
%Estimate total chromatid length based on the ratio
totConLen = totSegRatio * segConLen;

%Save the results in a xls file
if(fIndex == options.tinit) %Titlte of the colume
    xlswrite(xlsFileName,{'Filename'},1,'A1');
    xlswrite(xlsFileName,{'Length_Seg_Cond'},1,'B1');
    xlswrite(xlsFileName,{'Length_Tot_Cond'},1,'C1');
    xlswrite(xlsFileName,{'ShiftX'},1,'D1');
    xlswrite(xlsFileName,{'ShiftY'},1,'E1');
    xlswrite(xlsFileName,{'ShiftZ'},1,'F1');
end
xlswrite(xlsFileName,{regionName},1,strcat('A', num2str(fIndex+1)));
xlswrite(xlsFileName,segConLen,1,strcat('B', num2str(fIndex+1)));
xlswrite(xlsFileName,totConLen,1,strcat('C', num2str(fIndex+1)));
xlswrite(xlsFileName,options.xShift,1,strcat('D', num2str(fIndex+1)));
xlswrite(xlsFileName,options.yShift,1,strcat('E', num2str(fIndex+1)));
xlswrite(xlsFileName,options.zShift,1,strcat('F', num2str(fIndex+1)));

%Save image slices with segmentation results for varifications
conSlice = conStack(:,:,round(Nz/2));
imwrite(uint16(conSlice), fullfile(outDirSegVis, [regionName '.tif']), 'tif');
conSlice(nucRegion(:,:,round(Nz/2))==0)=0;
imwrite(uint16(conSlice), fullfile(outDirSegVis, [regionName '_seg.tif']), 'tif');

end