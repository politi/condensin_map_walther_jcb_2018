%% Change the file path to process specific directories
currdir = fileparts(mfilename('fullpath'));
addpath(fullfile(currdir, 'segmentation'), '-end');
addpath(fullfile(currdir, 'segmentation_external'), '-end');

cd(currdir)
indir = '../data/';
outdir = '../results/';
if ~exist(outdir, 'dir')
    mkdir(outdir);
end

indir_exp = {fullfile(indir, '20170522_CondensinClones_ExpA\Calibration\POI_meta')};
outdir_exp = {fullfile(outdir, '20170522_CondensinClones_ExpA\Calibration\POI_meta')};
 for idir = 1:length(indir_exp)
    expdir = indir_exp{idir};
    d = dir(expdir);
    isub = [d(:).isdir]; %# returns logical vector
    nameFolds = {d(isub).name}';
    nameFolds(ismember(nameFolds,{'.','..'})) = [];
    for celldir = nameFolds'
        indir_w = fullfile(expdir, celldir{1});
        outdir_w = fullfile(outdir_exp{idir}, celldir{1});
        if ~exist(outdir_w, 'dir')
            mkdir(outdir_w)
        end
        ls_segment_landmarks_single_file_main(indir_w, outdir_w);
    end
 end
