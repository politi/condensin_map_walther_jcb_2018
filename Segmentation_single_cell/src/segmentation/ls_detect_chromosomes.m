function [procStat] = ls_detect_chromosomes(curInDir, curOutDir, segParamsDir, exParamsFn, chrChanIdx, dispSegMass, nRemLowerSlices, varargin)
% LS_DETECT_CHROMOSOMES
% This function segments chromosome from chrChan and save chromsome chrMarker for cell boundary detection
% curInDir: full path to images containing time lapse
% curOutDir: full path to output directory where to save chromosome markers
% chrChaIdx: Index of chromosome channel
% exParamsFn: Name of the file storing the extracted parameters.

% Author: Julius Hossain, EMBL, Heidelberg, Germany: julius.hossain@embl.de
% Created: 2014-07-25
% Last update: 2017-06-12

close all;
procStat = 0; %Should go through the last part of the code to be set as 1
disp(['PROCESSING:' curInDir]);
lsmFn = dir([curInDir '*.lsm']);

tMax = length(lsmFn); %Number of lsm file in the folder
if tMax ==0
    return;
end
saveMarker = 0;

if length(varargin)> 0
    curOutDirMarker = varargin{1};
    saveMarker = 1;
end
if dispSegMass == 1
    dispDir = fullfile(curOutDir, 'DispSegMass', filesep);
    if ~exist(dispDir)
        mkdir(dispDir);
    end
end

%Filtering parameters
sigmaBlur = 3; %Sigma for Gaussian filter
hSizeBlur = 5; %Kernel size for Gaussian filter
bFactor2D = 0.33; %Contribution of 2D threshold
splitVol = 2000; %Minimum volume for to be considered for splitting
mergeVol = 150;  %Maximum volume of a blob that is considered for merging
minProphaseVol = 1350;
maxCentDisp = 11;
maxNoInitObj = 450;
tinit = 1;

% extract image dimensions
lsminf = lsminfo([curInDir lsmFn(1).name]); %Reads the lsm header
dx = lsminf.DIMENSIONS(1); %Image height
dy = lsminf.DIMENSIONS(2); % Image width
NzOrig = lsminf.DIMENSIONS(3); %Number of Z slices in lsm file
Nc = lsminf.NUMBER_OF_CHANNELS; %Number of channels

zinit = 1; %Set higher than one if you want to some of the lower slices
%Size of voxels in x, y and z in micro meter
voxelSizeX = lsminf.VoxelSizeX * 1e6;
voxelSizeY = lsminf.VoxelSizeY * 1e6;
voxelSizeZ = lsminf.VoxelSizeZ * 1e6;

zFactor = round(voxelSizeZ/voxelSizeX); % zFactor-1 intermediate slice(s) will be generated
voxelSizeZ = voxelSizeZ/zFactor; % Update voxelSizeZ based on the number of intermediate slices
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ; %Volume of a voxel in cubic micro meter
Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1; %Number of z slice after intepolation
chrStackOrig = zeros(dx,dy,NzOrig-zinit+1); % Allocate memomry for original stack

inHomCount = 0; % Defines the number of times we need to apply fn_InhomToHom(stk)
inHomCheck = 1; % Flag whether to check homogeinity or not

paramsName = {'ChrVolMic','NumChr'};
paramsTable = cell2table(cell(1,length(paramsName)));
paramsTable.Properties.VariableNames = paramsName;

tpoint = tinit;
while tpoint <=tMax
    %Reads input stack
    for zplane=zinit:NzOrig
        stacklsm = tiffread31([curInDir lsmFn(tpoint).name], 2*(zplane-zinit+1)-1);
        if Nc >= chrChanIdx
            chrStackOrig(:,:,zplane-zinit+1) = stacklsm.data{chrChanIdx};
        else
            disp('Chromosome channel is not found');
            return;
        end
        clear stacklsm
    end
    
    %Diplay input image stack
    %displaySubplots(chrStackOrig, 'Display input image stack', disRow, disCol, NzOrig, 1, 2);
    disp(['Processing: ' lsmFn(tpoint).name]);
    
    chrStack = ls_gen_intermediate_slices(chrStackOrig, zFactor);
    clear chrStackOrig;
    
    if tpoint == tinit
        chrRegion = zeros(dx,dy,Nz);%Store the detected chromosome region
        chrRegionCur1 = zeros(dx,dy,Nz);
        chrRegionCur2 = zeros(dx,dy,Nz);
        chrRegionPrev1 = zeros(dx,dy,Nz);
        bordImg = zeros(dx,dy,Nz);
        bordImg(1,:,:) = 1; bordImg(end,:,:) = 1;
        bordImg(:,1,:) = 1; bordImg(:,end,:) = 1;
        imgCentX = (dx+1)/2;
        imgCentY = (dy+1)/2;
        chromosomes = zeros(dx,dy,Nz);
    end
    %Display filtered data
    %displaySubplots(chrStack, 'Filtered chromosome', disRow, disCol, Nz, zFactor, 2);
    
    %% Detection of chromosome
    chrStack = imgaussian(chrStack, sigmaBlur, hSizeBlur);
    
    % This part deals with inhomogeinity where intensity of chr of interest is dim
    for iter = 1:inHomCount
        chrStack = ls_inhomogeneous_to_homogeneous(chrStack);
    end
    
    [chrThresh3D, chrThresh2D] = ls_get_thresholds(chrStack);
    
    for i = 1:Nz
        chrRegion(:,:,i) = double(chrStack(:,:,i) >= (chrThresh3D * (1-bFactor2D) + chrThresh2D(i) * bFactor2D));
        chrRegion(:,:,i)=imfill(chrRegion(:,:,i),'holes');
    end
    
    threeDLabel = bwconncomp(chrRegion,18);
    nCompI = threeDLabel.NumObjects;
    %disp(sprintf('Number of connected components is: %g', nCompI));
    
    %Check whether it went too low
    if inHomCheck == 1 && tpoint == tinit && nCompI >= maxNoInitObj && inHomCount >=1
        inHomCount = inHomCount - 1;
        inHomCheck = 0;
        fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'a');
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'nCompI:', nCompI);
        fclose(fileID);
        continue;
    end
    
    if (inHomCheck == 0 || tpoint == tinit) && nCompI >= maxNoInitObj
        [chrRegion, chrThresh3D, chrThresh2D, threeDLabel]= ls_adjust_thresholds_up(chrStack, chrThresh3D, chrThresh2D, maxNoInitObj, bFactor2D, threeDLabel);
    end
    
    nCompF = threeDLabel.NumObjects;
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    [initVol, idx] = max(numPixels);
    if threeDLabel.NumObjects > maxNoInitObj * 0.10 %This is just to speed up the process
        chrRegion(:,:,:) = 0;
        for i=1:min(round(maxNoInitObj * 0.10),threeDLabel.NumObjects)
            [~, idx] = max(numPixels);
            chrRegion(threeDLabel.PixelIdxList{idx}) = 1;
            numPixels(idx) = 0;
        end
    end
    
    initVol = initVol * voxelSize;
    
    if inHomCheck == 1 && tpoint == tinit && initVol < 3*mergeVol
        if inHomCount == 0
            fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'w');
        else
            fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'a');
        end
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'initVol:', initVol);
        fclose(fileID);
        inHomCount = inHomCount + 1;
        continue;
    end
    
    %fn_PC_DisplaySubplots(chrRegion, 'Detected chromosome', disRow, disCol, Nz, zFactor, 2);
    %disp(sprintf('chrThresh3D = %g', chrThresh3D));
    
    chrMarker = ls_split_connected_chromosomes(chrRegion, voxelSizeX, voxelSizeY, voxelSizeZ, bordImg, splitVol, mergeVol);
    %displaySubplots(chrMarker, 'Chromosome - initial chrMarker after split', disRow, disCol, Nz, zFactor, 2);
    
    chrMarker = ls_merge_small_regions(chrRegion, chrMarker, bordImg, voxelSizeX, voxelSizeY, voxelSizeZ, mergeVol);
    %displaySubplots(chrMarker, 'chrMarker image - merged', disRow, disCol, Nz, zFactor, 2);
    
    chrMarker = ls_remove_small_objects(chrMarker, 18, mergeVol, bordImg, voxelSize);
    %Display chrMarker image - refined
    %fn_PC_DisplaySubplots(chrMarker, 'chrMarker image - refined', disRow, disCol, Nz, zFactor, 2);
    
    %%
    threeDLabel = bwconncomp(chrMarker,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    %Use location information to select the nucleus of interest
    stat = regionprops(threeDLabel,'Centroid');
    
    if tpoint == tinit
        %Normalize the number of pixels based on three properties
        [chrRegionCur1, curCentX1, curCentY1, curCentZ1] = ls_detect_chromosome_in_first_stack(threeDLabel, stat, imgCentX, imgCentY, numPixels, dx, dy, Nz);
        
        distCent = sqrt((imgCentX-curCentX1)^2 + (imgCentY-curCentY1)^2) * voxelSizeX;
        ncVolume1 = sum(sum(sum(chrRegionCur1))) * voxelSize;
        %disp(sprintf('distCent = %g', distCent));
        
        if inHomCount == 0 && inHomCheck == 1
            fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'w');
        else
            fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'a');
        end
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
        fclose(fileID);
        
        if inHomCheck == 1
            if ncVolume1 > minProphaseVol * 2 && inHomCount >= 1
                inHomCount = inHomCount - 1;
                inHomCheck = 0;
                continue;
            elseif distCent > maxCentDisp || ncVolume1 < minProphaseVol
                inHomCount = inHomCount + 1;
                continue;
            else
                inHomCheck = 0;
            end
        end
        
        numChr = 1;
    else
        if numChr == 1
            prevCentX2 = prevCentX1;
            prevCentY2 = prevCentY1;
        end
        [chrRegionCur1, chrRegionCur2, curCentX1, curCentY1, curCentZ1, curCentX2, curCentY2, curCentZ2, numChr] = ls_detect_chromosome_in_later_stacks(threeDLabel, stat, chrRegionPrev1, prevCentX1, prevCentY1, prevCentX2, prevCentY2, numPixels, numChr, voxelSizeX, voxelSizeY, voxelSizeZ);
        
        ncVolume1 = sum(sum(sum(chrRegionCur1)))*voxelSize;
        ncVolume2 = sum(sum(sum(chrRegionCur2)))*voxelSize;
        fileID = fopen([segParamsDir lsmFn(tpoint).name(1:end-10) '_PAR.txt'],'a');
        fprintf(fileID,'%s %02d,   %s %05.f,   %s %04.f   %s %04.f   %s %04.f\n\r', 'tpoint:', tpoint, 'th3D:', chrThresh3D, 'volChr: ', ncVolume1+ncVolume2, 'nCompI: ', nCompI, 'nCompF: ', nCompF);
        fclose(fileID);
    end
    
    prevCentX1 = curCentX1;
    prevCentY1 = curCentY1;
    %prevCentZ1 = curCentZ1;
    
    chrRegionPrev1 = chrRegionCur1;
    
    if numChr == 2
        prevCentX2 = curCentX2;
        prevCentY2 = curCentY2;
        %prevCentZ2 = curCentZ2;
        %chrRegionPrev2 = chrRegionCur2;
    end
       
    chromosomes(:,:,:) = 0;
    chromosomes(chrRegionCur1(:,:,:) == 1) = 1;
    chromosomes(chrRegionCur2(:,:,:) == 1) = 2;
    if nRemLowerSlices > 0
        chromosomes(:,:,1:nRemLowerSlices*zFactor) = 0;
    end
    chrVolMic  = sum(sum(sum(chromosomes(:,:,:)>0))) * voxelSize;
    savefile = [curOutDir lsmFn(tpoint).name(1:end-4) '.mat'];
    chrMass = ls_remove_intermediate_slices(chromosomes, zFactor);
    
    save(savefile, 'chrMass', 'chrVolMic', 'numChr'); %Set 1 and 2 for first and second chromosome masses, res.
    
    newTable = cell2table({chrVolMic,numChr});
    newTable.Properties.VariableNames = paramsName;
    paramsTable = cat(1,paramsTable,newTable);
    
    savefile = [curOutDirMarker lsmFn(tpoint).name(1:end-4) '.mat'];
    if saveMarker == 1
        save(savefile, 'chrMarker', 'chromosomes', 'numChr', 'chrThresh3D', 'chrThresh2D');
    end
    
    % 8 corner points are set to 1 in order to keep effective volume same
    % in all timepoints
    if dispSegMass == 1
        chrRegionCur1(1,1,1) = 1;     chrRegionCur1(1,1,end) = 1; chrRegionCur1(1,end,1)   = 1;
        chrRegionCur1(1,end,end) = 1; chrRegionCur1(end,1,1) = 1; chrRegionCur1(end,1,end) = 1;
        chrRegionCur1(end,end,1) = 1; chrRegionCur1(end,end,end) = 1;
        
        [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
        
        hVol = figure('Name', strcat('Frame: ', num2str(tpoint, '%0.3f')), 'NumberTitle','off', 'Visible', 'off');
        isosurface(X,Y,Z,chrRegionCur1,0.9);
        alpha(0.5)
        isosurface(X,Y,Z,chrRegionCur2,0.7);
        alpha(0.7)
        axis equal
        
        savefile = [dispDir lsmFn(tpoint).name(1:end-4) '.jpg'];
        saveas(hVol, savefile, 'jpg');
        delete(hVol);
    end
   
    %%End of displaying volume and predicting parameters
    tpoint = tpoint + 1;
    
    clear maskedchrStack;
    clear wsLabel;
    clear distImage;
    clear chrMarker;
    clear X;
    clear Y;
    clear Z;
    clear contourCRegionThreeD;
    clear dumCol;
    clear hist;
    close all;
end
paramsFn = fullfile(curOutDir,  exParamsFn);
paramsTable(1,:) = [];
writetable(paramsTable,paramsFn,'Delimiter','\t');
procStat = 1;
end