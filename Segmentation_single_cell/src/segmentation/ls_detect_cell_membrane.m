function [procStat] = ls_detect_cell_membrane(curInDir, curOutDir, curOutDirMarker,...
    exParamsFn, cellChanIdx, chrChanIdx, nRemLowerSlices, chrSigInDxt, dispSegMass)
% curInDir: Input data directory
% curOutDir: Output data directory
% curOutDirMarker: Directory containing chromosome markers saved by ls_detect_chromosomes
% exParamsFn: Name of the file storing the extracted parameters.
% cellChanIdx: Channel index of dextran
% chrChanIdx: Channel index of chromosome
% nRemLowerSlices: Number of lower slices to be removed
% chrSigInDxt: Set 1 if chromosome signal is also prominent in dextral channel

% Author: Julius Hossain, EMBL, Heidelberg, Germany, julius.hossain@embl.de
% Created: 2014-11-13
% Last update: 2017-06-13
% This function detects cell boundary and extracts different parameters

close all;
disp(['PROCESSING:' curInDir]);
if dispSegMass == 1
    dispDir = fullfile(curOutDir, 'DispSegMass', filesep);
    if ~exist(dispDir)
        mkdir(dispDir);
    end
end

lsmFn = dir([curInDir '*.lsm']);
tMax = length(lsmFn); %Number of lsm file in the folder
matMarkerFn = dir([curOutDirMarker, '*.mat']);

if tMax ==0 || tMax ~= length(matMarkerFn)
    return;
end

paramsName = {'ChrVolMic', 'CellVolMic', 'NumChr'};
paramsTable = cell2table(cell(1,length(paramsName)));
paramsTable.Properties.VariableNames = paramsName;

%Structuring element
se = strel(ones(3,3,3));
%Define the maximum number of subplots to disply intermediate results
rCRegion = 1;
bFactorRatio = 0.20;
procStat = 0;

%Filtering parameters
sigmaBlur = 5; %Sigma for negative staining channel
hSizeBlur = 7; %Kernel size for negative staining channel

sigmaRatio=3; %Sigma for gaussian blur in ratio image.
hSizeRatio=5; %Kernel size for gaussian blur in ratio image

sigmaChSeg=3; %Sigma for gaussian blur in chromosome segmentation
hSizeChSeg=5; %Kernel size for gaussian blur in chromosome segmentation
bFactorCh2D = 0.33;
minVol = 500;

color = {[0.05 0.5 0.15], [0.55 0.1 0.1]};
thresh = [0.5 0.5];
alpha = [0.45 0.99];

lsminf = lsminfo([curInDir lsmFn(1).name]); %lsm header
dx = lsminf.DIMENSIONS(1);
dy = lsminf.DIMENSIONS(2);
NzOrig = lsminf.DIMENSIONS(3); %number of Z slices in lsm file
Nc = lsminf.NUMBER_OF_CHANNELS; %number of channels

tinit = 1; % Set higher than one if you do not want to process some timepoints before tinit
zinit = 1; % Set higher than one if you do not want to process some lower slices

voxelSizeX = lsminf.VoxelSizeX * 1e6;
voxelSizeY = lsminf.VoxelSizeY * 1e6;
voxelSizeZ = lsminf.VoxelSizeZ * 1e6;

zFactor = round(voxelSizeZ/voxelSizeX);
voxelSizeZ = voxelSizeZ/zFactor; % voxelsize in Z gets closer to that in XY
voxelSize = voxelSizeX * voxelSizeY * voxelSizeZ;

Nz = (NzOrig-zinit+1) * zFactor - zFactor + 1;
if Nc>=3
    chStackOrig = zeros(dx,dy,NzOrig-zinit+1);
    negStackOrig = zeros(dx,dy,NzOrig-zinit+1);
else
    disp('Stack contains less than three channels');
    return;
end

tpoint = tinit;
while tpoint <= tMax
    chrPack = load([curOutDirMarker matMarkerFn(tpoint).name]);
    
    for zplane=zinit:NzOrig
        stacklsm = tiffread31([curInDir lsmFn(tpoint).name],2*(zplane-zinit+1)-1);
        if Nc >= 3
            chStackOrig(:,:,zplane-zinit+1) = stacklsm.data{chrChanIdx};
            negStackOrig(:,:,zplane-zinit+1) = stacklsm.data{cellChanIdx};
        end
        clear stacklsm
    end
    
    disp(['Processing: ' lsmFn(tpoint).name]);
    chStack = ls_gen_intermediate_slices(chStackOrig, zFactor);
    negStack = ls_gen_intermediate_slices(negStackOrig, zFactor);
    
    chStackBack  = chStack;
    negStackBack = negStack;
    
    if tpoint == tinit
        tempVol = zeros(dx,dy,Nz);   %Store current segmented volume  as a referece for next/prev time point
        cRegion = zeros(dx,dy,Nz);   %Store the detected cytoplasm region
        chRegion = zeros(dx,dy,Nz);  %Store the detected chromosome region
    end
    
    chStack = imgaussian(chStack,sigmaBlur,hSizeBlur);
    negStack = imgaussian(negStack, sigmaBlur, hSizeBlur);
    
    %% Detection of chromosome
    chStackSeg = imgaussian(chStackBack, sigmaChSeg, hSizeChSeg);
    %[chThresh3D, histCh] = Otsu_3D_Img(chStackSeg, 0);
    chThresh2D = chrPack.chrThresh2D;
    chThresh3D = chrPack.chrThresh3D;
    if chrSigInDxt == 1
        chRegion(:,:,:) = 0;
        %Update threshold for different slices
        for i = 1:Nz
            chRegion(:,:,i) = double(chStackSeg(:,:,i) >= (chThresh3D * (1-bFactorCh2D) + chThresh2D(i) * bFactorCh2D));
        end
        
        chStackNorm = chStack;
        chStackNorm(chRegion(:,:,:) == 1) = 0;
        sumBgAvgInt = 0;
        for i = 1: Nz
            totalBgInt = sum(sum(chStackNorm(:,:,i)));
            totalBgPix = sum(sum(chRegion(:,:,i)==0));
            avgBgInt = totalBgInt/totalBgPix;
            tempFrame = chStack(:,:,i);
            tempFrame(chStackSeg(:,:,i) < avgBgInt) = avgBgInt;
            chStackNorm(:,:,i) = tempFrame;
            sumBgAvgInt = sumBgAvgInt + avgBgInt;
        end
        gAvgBgInt = sumBgAvgInt/Nz;
        ratioImage = (gAvgBgInt * negStack)./chStackNorm;
        
        ratioImage = imgaussian(ratioImage, sigmaRatio, hSizeRatio);
        clear chStackNorm;
        clear tempFrame;
    else
        ratioImage = negStack;
    end
    
    [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    negNumVoxels = dx*dy*Nz*0.01;
    sumVoxels = 0;
    for i = length(histRatio):-1: 1
        sumVoxels = sumVoxels + histRatio(i);
        if sumVoxels >= negNumVoxels
            break;
        end
    end
    
    ratioImage = i - ratioImage;
    ratioImage(ratioImage(:,:,:) <0) = 0;
    
    if nRemLowerSlices > 0
        ratioImage(:,:,1:nRemLowerSlices*zFactor) = 0;
        [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 1);
    else
        [cThresh3D, histRatio] = ls_otsu_3d_image(ratioImage, 0);
    end
    
    
    cRegion(:,:,:) = 0;
    for i = 1:Nz
        cThresh2D = ls_otsu_2d(ratioImage(:,:,i));
        cRegion(:,:,i) = double(ratioImage(:,:,i) >= (cThresh3D * (1-bFactorRatio) + cThresh2D * bFactorRatio));
    end
    
    bgMaskInt = cRegion;
    threeDLabel = bwconncomp(cRegion,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    for i = 1 : numel(numPixels)
        if numPixels(i) * voxelSize < minVol
            cRegion(threeDLabel.PixelIdxList{i}) = 0;
        end
    end
    
    %% Filling of thresholded image
    for zplane = 1: Nz
        cRegion(:,:,zplane)=imdilate(cRegion(:,:,zplane),strel('disk',rCRegion,0));
        cRegion(:,:,zplane)=imfill(cRegion(:,:,zplane),'holes');
        bgMaskInt(:,:,zplane)=imdilate(bgMaskInt(:,:,zplane),strel('disk',rCRegion,0));
        bgMaskInt(:,:,zplane)=imfill(bgMaskInt(:,:,zplane),'holes');
        for i=1:rCRegion
            cRegion(:,:,zplane)=imerode(cRegion(:,:,zplane),strel('diamond',1));
        end
    end
    
    bgMaskInt = 1-bgMaskInt;
    bgStack = negStackBack;
    bgStack(bgMaskInt(:,:,:) == 0) = 0;
    totBgInt = sum(sum(sum((bgStack(:,:,round(Nz*0.25):round(Nz*0.75))))));
    totBgPix = sum(sum(sum((bgMaskInt(:,:,round(Nz*0.25):round(Nz*0.75))))));
    
    %% Combining distance transform and watershed to identify cell region of interest
    distImage = bwdistsc(~cRegion,[1 1 voxelSizeZ/voxelSizeX]);
    
    cRegionEroded = imerode(cRegion, se);
    
    chrPack.chrMarker(cRegionEroded(:,:,:) == 0) = 0;
    chrPack.chrMarker(:,1,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(1,:,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(:,dy,round(Nz/20):round(Nz/2)) = 1;
    chrPack.chrMarker(dx,:,round(Nz/20):round(Nz/2)) = 1;
    
    clear cRegionEroded;
    
    distImage = -distImage; % invert distance image
    %displaySubplots(distImage, 'DT of inverse', disRow, disCol, Nz, zFactor, 2);
    distImage = imimposemin(distImage,chrPack.chrMarker, 18); %suppress local minima other than markers
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression', disRow, disCol, Nz, zFactor, 2);
    distImage(~cRegion) = -Inf; % Set outside of cell region with infinity
    %fn_PC_DisplaySubplots(distImage, 'DT after suppression - inf', disRow, disCol, Nz, zFactor, 2);
    wsLabel = watershed_old(distImage); %Apply watershed algorithm
    
    wsLabel(~cRegion) = 0;
    %fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    
    threeDLabel = bwconncomp(wsLabel,18);
    wsLabel(:,:,:) = 0;
    for i = 1: threeDLabel.NumObjects
        wsLabel(threeDLabel.PixelIdxList{i}) = i;
    end
    
    if chrPack.numChr == 1
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) ==1,  wsLabel(:,:,:) >=1))));
        lbl1 = round(labSum/labCount);
        wsLabel(wsLabel(:,:,:) ~= lbl1) = 0;
    else
        tempVol(:,:,:) = 0;
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 1))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 1, wsLabel(:,:,:) >=1))));
        lbl1 = round(labSum/labCount);
        
        tempVol(wsLabel(:,:,:) == lbl1) = 1;
        labSum = sum(sum(sum(wsLabel(chrPack.chromosomes(:,:,:) == 2))));
        labCount = sum(sum(sum(and(chrPack.chromosomes(:,:,:) == 2, wsLabel(:,:,:) >=1))));
        lbl2 = round(labSum/labCount);
        tempVol(wsLabel(:,:,:) == lbl2) = 1;
        
        wsLabel = tempVol;
    end
    wsLabel(wsLabel(:,:,:)>0) = 1;
    
    %fn_PC_DisplaySubplots(wsLabel, 'Watershed on distance image - background removed', disRow, disCol, Nz, zFactor, 2);
    wsLabel = imclose(wsLabel, se);
    %To deal with the filtering
    wsLabel = imclose(wsLabel,se);
    wsLabel = imdilate(wsLabel,se);
    
    %Store  initial detected cell region as 'cRegion'
    cRegion = wsLabel;
    %displaySubplots(cRegion, 'Selected blob using Watershed + DT raw', disRow, disCol, Nz, zFactor, 2);
    
    cRegionDis = cRegion;
    cRegion(:,:,:) = 0;
    for zplane=1:Nz
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
        if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=10
            continue;
        end
        
        cRegionDis(:,:,zplane) = imsubtract(cRegionDis(:,:,zplane), curBlob);
        cRegion(:,:,zplane) = curBlob;
        
        curBlob = ls_find_biggest_object(cRegionDis(:,:,zplane),8);
        if sum(sum(curBlob))*voxelSizeX*voxelSizeY <=10
            continue;
        end
        cRegion(:,:,zplane) = or(cRegion(:,:,zplane), curBlob);
    end
    threeDLabel = bwconncomp(cRegion,18);
    numPixels = cellfun(@numel,threeDLabel.PixelIdxList);
    [~, idx] = max(numPixels);
    cRegion(:,:,:) = 0;
    cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    numPixels(idx) = 0;
    [biggest, idx] = max(numPixels);
    if biggest *voxelSizeX * voxelSizeY * voxelSizeZ >=1000
        cRegion(threeDLabel.PixelIdxList{idx}) = 1;
    end
    %% End of DT + WS
    refVol = sum(sum(sum(cRegion))) * voxelSizeX * voxelSizeY * voxelSizeZ;
    cRegion = ls_smooth_and_equalize(cRegion, refVol, voxelSizeX*voxelSizeY*voxelSizeZ);
    
    for i=1:Nz
        cRegion(:,:,i) = imfill(cRegion(:,:,i), 'holes');
    end
    
    for i = round(Nz*0.7):Nz-1
        cRegion(:,:,i+1) = double(and(cRegion(:,:,i+1), cRegion(:,:,i)));
    end
    chrPack.chromosomes(cRegion(:,:,:) == 0) = 0;
    
    %% Save results in mat file
    chrVolMic  = sum(sum(sum(chrPack.chromosomes(:,:,:)>0))) * voxelSize;
    cellVolMic = sum(sum(sum(cRegion(:,:,:)>0))) * voxelSize;
    cellMass = ls_remove_intermediate_slices(cRegion, zFactor);
    chrMass = ls_remove_intermediate_slices(chrPack.chromosomes, zFactor);
    numChr = chrPack.numChr;
    
    savefile = [curOutDir matMarkerFn(tpoint).name];
    save(savefile,'cellMass', 'chrMass', 'chrVolMic', 'cellVolMic', 'numChr');
    
    newTable = cell2table({chrVolMic,cellVolMic, numChr});
    newTable.Properties.VariableNames = paramsName;
    paramsTable = cat(1,paramsTable,newTable);
    
    if dispSegMass == 1
        [X,Y,Z]=meshgrid((1:dy)*voxelSizeY,(1:dx)*voxelSizeX,(1:Nz)*voxelSizeZ);
        hVol = figure('Name', strcat('Frame: ', num2str(tpoint, '%0.3f')), 'NumberTitle','off', 'Visible', 'off');
        hVol = fn_plot_3d_chromosome_cell_surfaces(hVol, X, Y, Z, cRegion, chrPack.chromosomes, thresh, color, alpha);
        
        savefile = [dispDir lsmFn(tpoint).name(1:end-4) '.jpg'];
        saveas(hVol, savefile, 'jpg');
        delete(hVol);
    end
    
    clear cellVolume;
    clear nucVolume;
    clear cellRegion;
    clear chrRegion;
    clear numChr;
    clear wsLabel;
    clear distImage;
    clear chrMarker;
    clear hist;
    close all;
    clear chrPack;
    tpoint = tpoint + 1;
end

paramsFn = fullfile(curOutDir,  exParamsFn);
paramsTable(1,:) = [];
writetable(paramsTable,paramsFn,'Delimiter','\t');
procStat = 1;
end